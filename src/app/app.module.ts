import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Sample1Component } from './sample1/sample1.component';
import { LeftComponent } from './sample1/left/left.component';
import { RightComponent } from './sample1/right/right.component';
import { Sample0Component } from './sample0/sample0.component';

import { DragulaModule } from 'ng2-dragula';

@NgModule({
  declarations: [
    AppComponent,
    LeftComponent,
    RightComponent,
    Sample1Component,
    Sample0Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    DragulaModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
