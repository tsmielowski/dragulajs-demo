import { Component, OnInit } from '@angular/core';
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: 'app-sample1',
  templateUrl: './sample1.component.html',
  styleUrls: ['./sample1.component.css']
})
export class Sample1Component implements OnInit {

  readonly name: string = 'sample';

  constructor(
    private dragulaService: DragulaService
  ) { }

  ngOnInit(): void {
    this.createGroup();
  }

  private createGroup(): void {
    this.dragulaService.find(this.name) || this.dragulaService.createGroup(this.name, {
      accepts: (el: Element, target: Element, source: Element, sibling: Element | null): boolean => {
        return sibling !== null && sibling.getAttribute('data') === 'drop';
      },
      copy: (el: Element, container: Element): boolean => container.id === 'left',
      moves: (el: Element, container: Element): boolean => container.id === 'left'
    });
  }

}
