import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Sample0Component } from './sample0.component';

describe('Sample0Component', () => {
  let component: Sample0Component;
  let fixture: ComponentFixture<Sample0Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Sample0Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Sample0Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
