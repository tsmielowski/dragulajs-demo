import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Sample0Component } from './sample0/sample0.component';
import { Sample1Component } from './sample1/sample1.component';

const routes: Routes = [
  { path: '', component: Sample0Component },
  { path: 'sample1', component: Sample1Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
